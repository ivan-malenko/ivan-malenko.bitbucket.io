const LOCALE_EN = 'en-US';
const LOCALE_RU = 'ru-RU';
const LOCALE_DEFAULT = LOCALE_RU;
const DAYS_IN_WEEK = 7;

const daysOfWeek = (locale = LOCALE_DEFAULT) => {
    let date = new Date;

    date.setDate(date.getDate() - date.getDay());

    let days = [...new Array(DAYS_IN_WEEK)]
        .map(() => {
            let day = date.toLocaleString(locale, {weekday: 'short'});
            date.setDate(date.getDate() + 1);
            return day;
        });

    if (locale === LOCALE_RU) days.splice(DAYS_IN_WEEK, 0, days.splice(0, 1)[0]);

    return days;
};

const startOfMonth = (dateSource, locale = LOCALE_DEFAULT) => {
    let date = new Date(dateSource);
    date.setDate(1);
    return date;
};

const endOfMonth = (dateSource, locale = LOCALE_DEFAULT) => {
    let date = new Date(dateSource);
    date.setMonth(date.getMonth() + 1);
    date = startOfMonth(date, locale);
    date.setDate(date.getDate() - 1);
    return date;
};

const startOfWeek = (dateSource, locale = LOCALE_DEFAULT) => {
    let date = new Date(dateSource);
    let localeOffset = locale === LOCALE_RU ? 1 : 0;
    date.setDate(date.getDate() - date.getDay() + localeOffset);
    return date;
};

const endOfWeek = (dateSource, locale = LOCALE_DEFAULT) => {
    let date = startOfWeek(dateSource, locale);
    date.setDate(date.getDate() + DAYS_IN_WEEK - 1);
    return date;
};

const createCalendar = (element, year, month, locale = LOCALE_DEFAULT) => {
    let currentDate = new Date(year, month);
    let currentMonth = currentDate.getMonth();
    let dates = [];

    let monthStart = startOfMonth(currentDate, locale);
    let monthEnd = endOfMonth(currentDate, locale);
    let startDate = startOfWeek(monthStart, locale);
    let endDate = endOfWeek(monthEnd, locale);

    while (startDate <= endDate) {
        dates.push({
            isCurrentMonth: currentMonth === startDate.getMonth(),
            date:           startDate.getDate(),
        });
        startDate.setDate(startDate.getDate() + 1);
    }

    let calendar = document.createElement('table');

    calendar.appendChild(renderCalendarHeader(daysOfWeek(locale)));
    calendar.appendChild(renderCalendarBody(dates));

    element.innerHTML = '';
    element.appendChild(calendar);
};

const renderCalendarHeader = weekDays => {
    let thead = document.createElement('thead');
    let tr = document.createElement('tr');

    weekDays.forEach(day => {
        let td = document.createElement('th');
        td.innerText = day;
        tr.appendChild(td);
    });

    thead.appendChild(tr);

    return thead;
};
const renderCalendarBody = dates => {
    let tbody = document.createElement('tbody');

    for (let weekIdx = 0; weekIdx < dates.length; weekIdx += DAYS_IN_WEEK) {
        let tr = document.createElement('tr');

        dates.slice(weekIdx, weekIdx + DAYS_IN_WEEK).map(({isCurrentMonth, date}) => {
            let td = document.createElement('td');
            isCurrentMonth && td.classList.add('current-month');
            td.innerText = date;
            tr.appendChild(td);
        });

        tbody.appendChild(tr);
    }

    return tbody;
};

let viewYear = (new Date).getFullYear();
let viewMonth = (new Date).getMonth();
let viewLocale = LOCALE_DEFAULT;

const setViewMonth = month => {
    let date = new Date(viewYear, viewMonth);
    date.setMonth(month);
    viewYear = date.getFullYear();
    viewMonth = date.getMonth();
};

const calendarHolder = document.querySelector('#calendar');

document.querySelector('#prevMonth').addEventListener('click', () => {
    setViewMonth(viewMonth - 1);
    createCalendar(calendarHolder, viewYear, viewMonth, viewLocale);
});
document.querySelector('#nextMonth').addEventListener('click', () => {
    setViewMonth(viewMonth + 1);
    createCalendar(calendarHolder, viewYear, viewMonth, viewLocale);
});
document.querySelector('#localeEN').addEventListener('click', () => {
    if (viewLocale === LOCALE_EN) return;

    viewLocale = LOCALE_EN;

    createCalendar(calendarHolder, viewYear, viewMonth, viewLocale);
});
document.querySelector('#localeRU').addEventListener('click', () => {
    if (viewLocale === LOCALE_RU) return;

    viewLocale = LOCALE_RU;

    createCalendar(calendarHolder, viewYear, viewMonth, viewLocale);
});

createCalendar(calendarHolder, viewYear, viewMonth, viewLocale);