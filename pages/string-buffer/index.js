const makeBuffer = () => {
    let buffer = '';
    let bufferAppend = (str = '') => buffer = `${buffer}${str}`;

    bufferAppend.clear = () => buffer = '';

    return bufferAppend;
};