describe('', function () {
    it('leaves an empty line as is', function () {
        assert.equal(compress(''), '');
    });

    it('a string is larger if one character at a time', function () {
        assert.equal(compress('abcdef'), '1a1b1c1d1e1f');
    });

    it('spaces are counted', function () {
        assert.equal(compress('ab    cd'), '1a1b4 1c1d');
    });

    it('normal string is compressed', function () {
        assert.equal(compress('aaaaaabbbccbbddddd'), '6a3b2c2b5d');
    });
});