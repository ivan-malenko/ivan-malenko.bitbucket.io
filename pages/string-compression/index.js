const compress = str => {
    let result = '';
    let counter = 1;

    for (let idx = 0; idx < str.length; idx++) {
        if (str[idx] === str[idx + 1]) {
            counter++;
            continue;
        }

        result = `${result}${counter}${str[idx]}`;
        counter = 1;
    }

    return result;
};