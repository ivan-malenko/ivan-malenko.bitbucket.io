let field = document.querySelector('#field');
let fieldBorderWidth = parseInt(window.getComputedStyle(field).borderWidth);
let ball = document.querySelector('#ball');
let ballR = ball.offsetWidth / 2;
let ballOffset = 2 * (fieldBorderWidth + ballR);

field.addEventListener('click', ({clientX, clientY}) => {
    let {
        top: fieldTop,
        left: fieldLeft,
        width: fieldWidth,
        height: fieldHeight,
    } = field.getBoundingClientRect();

    let ballMaxX = fieldWidth - ballOffset;
    let ballMaxY = fieldHeight - ballOffset;
    let ballXOffset = clientX - fieldLeft - fieldBorderWidth - ballR;
    let ballYOffset = clientY - fieldTop - fieldBorderWidth - ballR;

    if (ballXOffset < 0) {
        ballXOffset = 0;
    } else if (ballXOffset > ballMaxX) {
        ballXOffset = ballMaxX;
    }

    if (ballYOffset < 0) {
        ballYOffset = 0;
    } else if (ballYOffset > ballMaxY) {
        ballYOffset = ballMaxY;
    }

    ball.style.transform = `translate(${ballXOffset}px, ${ballYOffset}px)`;
});