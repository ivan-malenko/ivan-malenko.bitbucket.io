let data = {
    'Животные': {
        'Млекопитающие': {
            'Коровы': {},
            'Ослы': {},
            'Собаки': {},
            'Тигры': {},
        },
        'Другие': {
            'Змеи': {},
            'Птицы': {},
            'Ящерицы': {},
        },
    },

    'Рыбы': {
        'Аквариумные': {
            'Гуппи': {},
            'Скалярии': {},
        },
        'Морские': {
            'Морская форель': {},
        },
    },
};

function createTree(container, data, usingAPI = true) {
    usingAPI
        ? createTree_V_API(container, data)
        : createTree_V_STRING(container, data);
}

function createTree_V_API(container, data) {
    if (typeof data !== 'object') return;

    let ul = document.createElement('ul');

    Object.keys(data).forEach(key => {
        let li = document.createElement('li');
        li.innerText = key;
        createTree(li, data[key]);
        ul.appendChild(li);
        container.appendChild(ul);
    });
}

function createTree_V_STRING(container, data) {
    const renderLeaf = data => {
        if (typeof data !== 'object') return '';

        let ulContent = Object.entries(data)
            .reduce((acc, [title, leaf]) => `${acc}<li>${title}${renderLeaf(leaf)}</li>`, '');

        return ulContent ? `<ul>${ulContent}</ul>` : '';
    };

    container.innerHTML = renderLeaf(data);
}

createTree(document.getElementById('treeAPI'), data);
createTree(document.getElementById('treeString'), data, false);

document.addEventListener('click', ({target}) => {
    if (!target.closest('.tree') || target.tagName !== 'LI') {
        return;
    }

    let innerList = target.querySelector('ul');

    if (!innerList) {
        return;
    }

    innerList.hasAttribute('hidden')
        ? innerList.removeAttribute('hidden')
        : innerList.setAttribute('hidden', 'hidden');
});