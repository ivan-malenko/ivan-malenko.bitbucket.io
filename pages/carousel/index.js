[...document.querySelectorAll('.carousel')].forEach(carousel => {
    let carouselTrack = carousel.querySelector('.carousel-track');
    let carouselSlides = carousel.querySelectorAll('.carousel-slide');
    let carouselControlPrev = carousel.querySelector('.carousel-control--prev');
    let carouselControlNext = carousel.querySelector('.carousel-control--next');
    let carouselOffset = 0;
    let slidesPerScreen = 3;
    let slidesQty = carouselSlides.length;

    carouselControlPrev.addEventListener('click', () => {
        let slideWidth = carouselSlides[0].offsetWidth;

        if (!carouselOffset) {
            return;
        }

        carouselOffset += slideWidth;
        carouselTrack.style.transform = `translateX(${carouselOffset}px)`;
    });
    carouselControlNext.addEventListener('click', () => {
        let slideWidth = carouselSlides[0].offsetWidth;

        if (-carouselOffset === slideWidth * (slidesQty - slidesPerScreen)) {
            return;
        }

        carouselOffset -= slideWidth;
        carouselTrack.style.transform = `translateX(${carouselOffset}px)`;
    });
});