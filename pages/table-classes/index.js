const MIN_AGE = 18;
const GENDER = {
    m: 'male',
    f: 'female',
};

function highlight(table) {
    [...table.querySelectorAll('tbody tr')].forEach(row => {
        if (row.querySelector('[data-available="true"]')) {
            row.classList.add('available');
        } else if (row.querySelector('[data-available="false"]')) {
            row.classList.add('unavailable');
        } else {
            row.hidden = true;
        }

        if (+(row.querySelector('td:nth-child(2)').innerText.trim()) < MIN_AGE) {
            row.style.textDecoration = 'line-through';
        }

        let genderClass = GENDER[row.querySelector('td:nth-child(3)').innerText.trim()];

        if (genderClass) {
            row.classList.add(genderClass);
        }
    });
}

highlight(document.querySelector('.js-teachers'));