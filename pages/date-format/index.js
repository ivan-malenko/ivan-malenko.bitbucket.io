const formatDate = date => {
    let year = date.getFullYear().toString().slice(2);
    let month = (date.getMonth() + 1).toString().padStart(2, 0);
    let dayDate = date.getDate().toString().padStart(2, 0);

    return `${dayDate}.${month}.${year}`;
};