function makeArmy() {
    let shooters = [];

    // 'while' fixed
    /*let i = 0;

    while (i < 10) {
      let shooter = i => () => alert(i);
      shooters.push(shooter(i));
      i++;
    }*/

    // 'for' used
    for (let i = 0; i < 10; i++) {
        let shooter = () => alert(i);
        shooters.push(shooter);
    }

    return shooters;
}