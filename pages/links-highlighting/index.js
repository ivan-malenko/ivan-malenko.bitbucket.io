document
    .querySelectorAll('ul a[href*="//"]:not([href^="http://internal.com"])')
    .forEach(anchor => anchor.classList.add('external'));