let tabsControls = [...document.querySelectorAll('.tabControl')];
let tabsContents = [...document.querySelectorAll('.tabContent')];

const setActiveTab = idx => {
    tabsControls.forEach(control => control.classList.remove('is-active'));
    tabsContents.forEach(content => content.setAttribute('hidden', ''));

    tabsControls[idx] && tabsControls[idx].classList.add('is-active');
    tabsContents[idx] && tabsContents[idx].removeAttribute('hidden');
};

setActiveTab(0);

tabsControls.forEach(tabControl => tabControl.addEventListener('click', () => {
    setActiveTab(tabsControls.indexOf(tabControl));
}));